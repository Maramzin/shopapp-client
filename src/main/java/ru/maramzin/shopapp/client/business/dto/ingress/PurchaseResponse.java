package ru.maramzin.shopapp.client.business.dto.ingress;

import java.util.UUID;

public record PurchaseResponse(UUID productId, String productName, Integer cost,
                               Integer discount, Integer quantity) {
}
