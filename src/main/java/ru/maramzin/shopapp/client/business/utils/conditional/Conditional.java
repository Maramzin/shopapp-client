package ru.maramzin.shopapp.client.business.utils.conditional;

public interface Conditional {

  Conditional then(VoidAction action);

  Conditional orElse(VoidAction action);

  Conditional orElseThrow(Throwable throwable);

  static Conditional of(boolean condition) {
    return ConditionalImpl.of(condition);
  }

  static ThrowableConditional ofThrowable(boolean condition) {
    return ThrowableConditionalImpl.of(condition);
  }
}
