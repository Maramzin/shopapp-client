package ru.maramzin.shopapp.client.business.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.maramzin.shopapp.client.business.converter.domain.RegisterAddressConverter;
import ru.maramzin.shopapp.client.business.domain.RegisterAddress;
import ru.maramzin.shopapp.client.business.dto.request.RegisterAddressRequest;
import ru.maramzin.shopapp.client.business.dto.response.Result;
import ru.maramzin.shopapp.client.business.dto.response.failed.RequestFailed;
import ru.maramzin.shopapp.client.business.dto.response.success.RegisterAddressResponse;
import ru.maramzin.shopapp.client.business.persistence.RegisterAddressRepository;

@Service
@RequiredArgsConstructor
public class RegisterAddressService implements RequestProcessor<RegisterAddressRequest, RegisterAddressResponse> {

  private final RegisterAddressRepository repository;
  private final RegisterAddressConverter converter;

  @Override
  @Transactional
  public Result<RegisterAddressResponse, RequestFailed> process(RegisterAddressRequest request) {
    RegisterAddress registerAddress = converter.convertToDomain(request)
        .validate()
        .execute()
        .ifSuccess(repository::persist)
        .rollbackIfFailed();

    return converter.convertToResult(registerAddress);
  }
}
