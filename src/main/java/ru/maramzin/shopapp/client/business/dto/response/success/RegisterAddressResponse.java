package ru.maramzin.shopapp.client.business.dto.response.success;

import lombok.Builder;
import lombok.Data;
import ru.maramzin.shopapp.client.business.dto.response.success.entity.AddressResponse;

@Data
@Builder
public class RegisterAddressResponse {

  AddressResponse addressInfo;
}
