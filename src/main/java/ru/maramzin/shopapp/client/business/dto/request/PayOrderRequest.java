package ru.maramzin.shopapp.client.business.dto.request;

import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PayOrderRequest {

  private UUID orderId;
}
