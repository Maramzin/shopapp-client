package ru.maramzin.shopapp.client.business.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@EqualsAndHashCode
@ToString
@Table(name = "item")
@Entity
public class Item {

  @Id
  private UUID id;

  private String productName;

  private Integer quantity;

  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "cart_id", nullable = false)
  private Cart cart;
}
