package ru.maramzin.shopapp.client.business.dto.ingress.enumeration;

public enum ClothesCategory {

  DRESS,
  SHOES,
  SHIRT,
  HAT,
  UNDERWEAR,
  JACKET
}
