package ru.maramzin.shopapp.client.business.converter.egress;

import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.client.business.converter.entity.EntityConverter;
import ru.maramzin.shopapp.client.business.dto.egress.PurchaseRequest;
import ru.maramzin.shopapp.client.business.entity.Item;

@Component
public class PurchaseConverter implements EntityConverter<Item, PurchaseRequest> {

  @Override
  public PurchaseRequest toResponse(Item entity) {
    return PurchaseRequest.builder()
        .productName(entity.getProductName())
        .quantity(entity.getQuantity())
        .build();
  }
}
