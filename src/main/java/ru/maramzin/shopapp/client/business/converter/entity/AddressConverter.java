package ru.maramzin.shopapp.client.business.converter.entity;

import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.client.business.dto.response.success.entity.AddressResponse;
import ru.maramzin.shopapp.client.business.entity.Address;

@Component
public class AddressConverter implements EntityConverter<Address, AddressResponse> {

  @Override
  public AddressResponse toResponse(Address entity) {
    return AddressResponse.builder()
        .id(entity.getId())
        .city(entity.getCity())
        .district(entity.getDistrict())
        .street(entity.getStreet())
        .house(entity.getHouse())
        .apartment(entity.getApartment())
        .zipcode(entity.getZipcode())
        .build();
  }
}
