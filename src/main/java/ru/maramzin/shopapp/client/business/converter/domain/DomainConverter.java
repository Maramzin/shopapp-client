package ru.maramzin.shopapp.client.business.converter.domain;

import ru.maramzin.shopapp.client.business.domain.BaseDomain;
import ru.maramzin.shopapp.client.business.dto.response.Result;
import ru.maramzin.shopapp.client.business.dto.response.failed.RequestFailed;

public interface DomainConverter <RQ, D extends BaseDomain<D>, RS> {
  D convertToDomain(RQ request);

  RS convertToResponse(D domain);

  default Result<RS, RequestFailed> convertToResult(D domain) {
    return new Result<>() {

      @Override
      public RS getSuccess() {
        return domain.isFailed() ? null : convertToResponse(domain);
      }

      @Override
      public RequestFailed getFailed() {
        return domain.isFailed() ? new RequestFailed(domain.getErrorMessage()) : null;
      }
    };
  }
}
