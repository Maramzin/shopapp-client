package ru.maramzin.shopapp.client.business.converter.entity;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.client.business.dto.response.success.entity.OrderResponse;
import ru.maramzin.shopapp.client.business.entity.Order;

@Component
@RequiredArgsConstructor
public class OrderConverter implements EntityConverter<Order, OrderResponse> {

  private final CartConverter cartConverter;
  private final AddressConverter addressConverter;

  @Override
  public OrderResponse toResponse(Order entity) {
    return OrderResponse.builder()
        .id(entity.getId())
        .creationTime(entity.getCreationTime())
        .deliveryStatus(entity.getDeliveryStatus())
        .deliveryPlace(entity.getDeliveryPlace())
        .paymentState(entity.getPaymentState())
        .cart(cartConverter.toResponse(entity.getCart()))
        .address(addressConverter.toResponse(entity.getAddress()))
        .clientId(entity.getClient().getId())
        .reservationId(entity.getReservationId())
        .build();
  }
}
