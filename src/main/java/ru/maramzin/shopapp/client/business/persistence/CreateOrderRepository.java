package ru.maramzin.shopapp.client.business.persistence;

import java.util.Optional;
import java.util.UUID;
import ru.maramzin.shopapp.client.business.domain.CreateOrder;
import ru.maramzin.shopapp.client.business.entity.Client;

public interface CreateOrderRepository extends Storage<CreateOrder> {

  void persist(CreateOrder domain);

  Optional<Client> findClientById(UUID id);
}
