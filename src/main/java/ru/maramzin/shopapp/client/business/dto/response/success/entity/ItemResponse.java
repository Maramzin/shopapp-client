package ru.maramzin.shopapp.client.business.dto.response.success.entity;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ItemResponse {

  private String productName;
  private Integer quantity;
}
