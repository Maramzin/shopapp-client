package ru.maramzin.shopapp.client.business.persistence;

import java.util.Optional;
import java.util.UUID;

public interface Searcher<E> {

  Optional<E> findById(UUID id);
}
