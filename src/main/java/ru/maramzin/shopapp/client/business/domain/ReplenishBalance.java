package ru.maramzin.shopapp.client.business.domain;

import java.util.Objects;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import ru.maramzin.shopapp.client.business.entity.Client;
import ru.maramzin.shopapp.client.business.utils.Action;
import ru.maramzin.shopapp.client.business.utils.exception.ValidationException;

@Getter
@Builder
public class ReplenishBalance extends BaseDomain<ReplenishBalance> {

  private Client client;

  @Getter(AccessLevel.NONE)
  private Integer amount;

  @Override
  public ReplenishBalance validate() {
    try {
      check(Objects.isNull(client), "Client does not exist.");
    } catch (ValidationException e) {
      errorMessage.append(e.getMessage());
    }

    return this;
  }

  @Override
  public ReplenishBalance execute() {
    if(!isFailed()) {
      client.setBalance(client.getBalance() + amount);
    }

    return this;
  }

  @Override
  public ReplenishBalance ifSuccess(Action<ReplenishBalance> action) {
    if(!isFailed()) {
      try {
        action.perform(this);
      } catch (Exception e) {
        errorMessage.append(e.getMessage());
      }
    }

    return this;
  }

  @Override
  public ReplenishBalance rollbackIfFailed() {
    if(isFailed()) {
      TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
    }

    return this;
  }
}
