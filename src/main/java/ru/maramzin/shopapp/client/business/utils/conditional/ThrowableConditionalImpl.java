package ru.maramzin.shopapp.client.business.utils.conditional;

import java.util.Objects;
import java.util.function.Consumer;

public class ThrowableConditionalImpl implements ThrowableConditional {

  private final boolean condition;
  private Throwable exception;

  private ThrowableConditionalImpl(boolean condition) {
    this.condition = condition;
  }

  public static ThrowableConditional of(boolean condition) {
    return new ThrowableConditionalImpl(condition);
  }

  public ThrowableConditional then(VoidAction action) {
    if(condition) {
      try {
        action.perform();
      } catch (Throwable e) {
        exception = e;
      }
    }

    return this;
  }

  public ThrowableConditional orElse(VoidAction action) {
    if(!condition) {
      try {
        action.perform();
      } catch (Throwable e) {
        exception = e;
      }
    }

    return this;
  }

  @Override
  public Conditional orElseThrow(Throwable throwable) {
    if(!condition) {
      throw new RuntimeException(throwable);
    }

    return this;
  }

  public ThrowableConditional onFailure(Consumer<Throwable> consumer) {
    if(Objects.nonNull(exception)) {
      consumer.accept(exception);
    }

    return this;
  }
}
