package ru.maramzin.shopapp.client.business.utils.exception;

public class ValidationException extends RuntimeException {

  public ValidationException(String message) {
    super(message);
  }
}
