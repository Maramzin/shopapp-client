package ru.maramzin.shopapp.client.business.dto.ingress.enumeration;

public enum SeasonCategory {

  WINTER,
  SPRING,
  SUMMER,
  AUTUMN
}
