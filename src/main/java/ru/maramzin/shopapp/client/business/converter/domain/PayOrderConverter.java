package ru.maramzin.shopapp.client.business.converter.domain;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.maramzin.shopapp.client.business.converter.entity.OrderConverter;
import ru.maramzin.shopapp.client.business.domain.PayOrder;
import ru.maramzin.shopapp.client.business.dto.request.PayOrderRequest;
import ru.maramzin.shopapp.client.business.dto.response.success.PayOrderResponse;
import ru.maramzin.shopapp.client.business.persistence.PayOrderRepository;

@Service
@RequiredArgsConstructor
public class PayOrderConverter implements DomainConverter<PayOrderRequest, PayOrder, PayOrderResponse> {

  private final OrderConverter orderConverter;
  private final PayOrderRepository repository;

  @Override
  public PayOrder convertToDomain(PayOrderRequest request) {
    return PayOrder.builder()
        .order(repository.findById(request.getOrderId()).orElse(null))
        .build();
  }

  @Override
  public PayOrderResponse convertToResponse(PayOrder domain) {
    return PayOrderResponse.builder()
        .orderInfo(orderConverter.toResponse(domain.getOrder()))
        .build();
  }
}
