package ru.maramzin.shopapp.client.business.adapter;

import ru.maramzin.shopapp.client.business.domain.PayOrder;

public interface PayOrderStorageAdapter {

  void commitPurchase(PayOrder domain);
}
