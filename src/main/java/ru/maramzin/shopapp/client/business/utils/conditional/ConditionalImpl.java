package ru.maramzin.shopapp.client.business.utils.conditional;

public class ConditionalImpl implements Conditional {
  private final boolean condition;

  private ConditionalImpl(boolean condition) {
    this.condition = condition;
  }

  public static Conditional of(boolean condition) {
    return new ConditionalImpl(condition);
  }

  public ConditionalImpl then(VoidAction action) {
    if(condition) {
      action.perform();
    }

    return this;
  }

  public ConditionalImpl orElse(VoidAction action) {
    if(!condition) {
      action.perform();
    }

    return this;
  }

  @Override
  public Conditional orElseThrow(Throwable throwable) {
    if(!condition) {
      throw new RuntimeException(throwable);
    }

    return this;
  }
}
