package ru.maramzin.shopapp.client.business.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.maramzin.shopapp.client.business.converter.domain.ReplenishBalanceConverter;
import ru.maramzin.shopapp.client.business.domain.ReplenishBalance;
import ru.maramzin.shopapp.client.business.dto.request.ReplenishBalanceRequest;
import ru.maramzin.shopapp.client.business.dto.response.Result;
import ru.maramzin.shopapp.client.business.dto.response.failed.RequestFailed;
import ru.maramzin.shopapp.client.business.dto.response.success.ReplenishBalanceResponse;
import ru.maramzin.shopapp.client.business.persistence.ReplenishBalanceRepository;

@Service
@RequiredArgsConstructor
public class ReplenishBalanceService implements RequestProcessor<ReplenishBalanceRequest, ReplenishBalanceResponse> {

  private final ReplenishBalanceRepository repository;
  private final ReplenishBalanceConverter converter;

  @Override
  @Transactional
  public Result<ReplenishBalanceResponse, RequestFailed> process(ReplenishBalanceRequest request) {
    ReplenishBalance domain = converter.convertToDomain(request)
        .validate()
        .execute()
        .ifSuccess(repository::persist)
        .rollbackIfFailed();

    return converter.convertToResult(domain);
  }
}
