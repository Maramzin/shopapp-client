package ru.maramzin.shopapp.client.business.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.maramzin.shopapp.client.business.converter.domain.RegisterClientConverter;
import ru.maramzin.shopapp.client.business.domain.RegisterClient;
import ru.maramzin.shopapp.client.business.dto.request.RegisterClientRequest;
import ru.maramzin.shopapp.client.business.dto.response.Result;
import ru.maramzin.shopapp.client.business.dto.response.failed.RequestFailed;
import ru.maramzin.shopapp.client.business.dto.response.success.RegisterClientResponse;
import ru.maramzin.shopapp.client.business.persistence.RegisterClientRepository;

@Service
@RequiredArgsConstructor
public class RegisterClientService implements RequestProcessor<RegisterClientRequest, RegisterClientResponse> {

  private final RegisterClientRepository repository;
  private final RegisterClientConverter converter;

  @Override
  @Transactional
  public Result<RegisterClientResponse, RequestFailed> process(RegisterClientRequest request) {
    RegisterClient registerClient = converter.convertToDomain(request)
        .validate()
        .execute()
        .ifSuccess(repository::persist)
        .rollbackIfFailed();

    return converter.convertToResult(registerClient);
  }
}
