package ru.maramzin.shopapp.client.business.dto.ingress.enumeration;

public enum ReservationStatus {
  OPENED,
  RETURNED,
  COMMITTED
}
