package ru.maramzin.shopapp.client.business.service;

import ru.maramzin.shopapp.client.business.dto.response.Result;
import ru.maramzin.shopapp.client.business.dto.response.failed.RequestFailed;

public interface RequestProcessor<RQ, RS> {

  Result<RS, RequestFailed> process(RQ request);
}
