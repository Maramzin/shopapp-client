package ru.maramzin.shopapp.client.business.persistence;

import ru.maramzin.shopapp.client.business.domain.RegisterAddress;

public interface RegisterAddressRepository extends Storage<RegisterAddress> {
}
