package ru.maramzin.shopapp.client.business.converter.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.client.business.dto.response.success.entity.CartResponse;
import ru.maramzin.shopapp.client.business.entity.Cart;
import ru.maramzin.shopapp.client.business.entity.Item;

@Component
@RequiredArgsConstructor
public class CartConverter implements EntityConverter<Cart, CartResponse> {

  private final ItemConverter itemConverter;

  @Override
  public CartResponse toResponse(Cart entity) {
    List<Item> items = Optional.ofNullable(entity.getItems()).orElse(new ArrayList<>());
    return CartResponse.builder()
        .items(items.stream()
            .map(itemConverter::toResponse)
            .toList())
        .build();
  }
}
