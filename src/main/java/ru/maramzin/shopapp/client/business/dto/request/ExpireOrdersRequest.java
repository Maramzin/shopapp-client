package ru.maramzin.shopapp.client.business.dto.request;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ExpireOrdersRequest {

  private LocalDateTime currentDateTime;
}
