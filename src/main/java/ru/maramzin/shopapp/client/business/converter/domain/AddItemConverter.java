package ru.maramzin.shopapp.client.business.converter.domain;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.maramzin.shopapp.client.business.converter.entity.CartConverter;
import ru.maramzin.shopapp.client.business.domain.AddItem;
import ru.maramzin.shopapp.client.business.dto.request.AddItemRequest;
import ru.maramzin.shopapp.client.business.dto.response.success.AddItemResponse;
import ru.maramzin.shopapp.client.business.persistence.AddItemRepository;

@Service
@RequiredArgsConstructor
public class AddItemConverter implements DomainConverter<AddItemRequest, AddItem, AddItemResponse> {

  private final AddItemRepository repository;
  private final CartConverter cartConverter;

  @Override
  public AddItem convertToDomain(AddItemRequest request) {
    return AddItem.builder()
        .client(repository.findClientById(request.getClientId()).orElse(null))
        .itemRequest(request.getItem())
        .build();
  }

  @Override
  public AddItemResponse convertToResponse(AddItem domain) {
    return AddItemResponse.builder()
        .cart(cartConverter.toResponse(domain.getCart()))
        .build();
  }
}
