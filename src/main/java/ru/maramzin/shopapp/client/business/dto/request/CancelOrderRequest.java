package ru.maramzin.shopapp.client.business.dto.request;

import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CancelOrderRequest {

  private UUID orderId;
}
