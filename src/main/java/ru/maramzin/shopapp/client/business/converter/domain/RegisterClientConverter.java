package ru.maramzin.shopapp.client.business.converter.domain;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.maramzin.shopapp.client.business.converter.entity.ClientConverter;
import ru.maramzin.shopapp.client.business.domain.RegisterClient;
import ru.maramzin.shopapp.client.business.dto.request.RegisterClientRequest;
import ru.maramzin.shopapp.client.business.dto.response.success.RegisterClientResponse;

@Service
@RequiredArgsConstructor
public class RegisterClientConverter implements DomainConverter<RegisterClientRequest,
    RegisterClient, RegisterClientResponse>{

  private final ClientConverter clientConverter;

  @Override
  public RegisterClient convertToDomain(RegisterClientRequest request) {
    return RegisterClient.builder()
        .request(request)
        .build();
  }

  @Override
  public RegisterClientResponse convertToResponse(RegisterClient domain) {
    return RegisterClientResponse.builder()
        .clientInfo(clientConverter.toResponse(domain.getClient()))
        .build();
  }
}
