package ru.maramzin.shopapp.client.business.dto.response.success;

import java.util.List;
import lombok.Builder;
import lombok.Data;
import ru.maramzin.shopapp.client.business.dto.response.success.entity.OrderResponse;

@Data
@Builder
public class ExpireOrdersResponse {
  private List<OrderResponse> orderResponses;
}
