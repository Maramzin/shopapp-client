package ru.maramzin.shopapp.client.business.dto.response.success.entity;

import java.util.UUID;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AddressResponse {

  private UUID id;
  private String city;
  private String district;
  private String street;
  private String house;
  private Integer apartment;
  private String zipcode;
}
