package ru.maramzin.shopapp.client.business.dto.response.success.entity;

import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CartResponse {

  private List<ItemResponse> items;
}
