package ru.maramzin.shopapp.client.business.dto.request;

import java.util.UUID;
import lombok.Data;
import ru.maramzin.shopapp.client.business.dto.request.entity.ItemRequest;

@Data
public class AddItemRequest {

  private UUID clientId;
  private ItemRequest item;
}
