package ru.maramzin.shopapp.client.business.domain;

import java.util.Objects;
import lombok.Builder;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import ru.maramzin.shopapp.client.business.entity.Order;
import ru.maramzin.shopapp.client.business.entity.enumeration.PaymentState;
import ru.maramzin.shopapp.client.business.utils.Action;
import ru.maramzin.shopapp.client.business.utils.exception.ValidationException;

@Builder
public class PayOrder extends BaseDomain<PayOrder> {

  private Order order;

  @Override
  public PayOrder validate() {
    try {
      check(Objects.isNull(order), "Order does not exist.");
      check(Objects.isNull(order.getClient()), "Client does not exist.");
      check(Objects.isNull(order.getCart()), "Cart does not exist.");
      check(Objects.isNull(order.getCart().getItems()), "Cart is empty.");
      check(order.getCart().getItems().isEmpty(), "Cart is empty.");
      check(PaymentState.PAID.equals(order.getPaymentState()), "Order is already paid.");
    } catch (ValidationException exception) {
      errorMessage.append(exception.getMessage());
    }

    return this;
  }

  @Override
  public PayOrder execute() {
    if(!isFailed()) {
      order.setPaymentState(PaymentState.PAID);
    }

    return this;
  }

  @Override
  public PayOrder ifSuccess(Action<PayOrder> action) {
    if(!isFailed()) {
      try {
        action.perform(this);
      } catch (Exception e) {
        errorMessage.append(e.getMessage());
      }
    }

    return this;
  }

  @Override
  public PayOrder rollbackIfFailed() {
    if(isFailed()) {
      TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
    }

    return this;
  }

  public Order getOrder() {
    return order;
  }
}
