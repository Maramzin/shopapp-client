package ru.maramzin.shopapp.client.business.domain;

import ru.maramzin.shopapp.client.business.utils.Action;
import ru.maramzin.shopapp.client.business.utils.StringUtils;
import ru.maramzin.shopapp.client.business.utils.exception.ValidationException;

public abstract class BaseDomain<D> {

  protected StringBuilder errorMessage = new StringBuilder();

  public String getErrorMessage() {
    return errorMessage.toString();
  }

  public boolean isFailed() {
    return StringUtils.isNotBlank(errorMessage.toString());
  }

  protected void check(Boolean condition, String message) {
    if(condition) {
      throw new ValidationException(message);
    }
  }

  public abstract D validate();

  public abstract D execute();

  public abstract D ifSuccess(Action<D> action);

  public abstract D rollbackIfFailed();
}
