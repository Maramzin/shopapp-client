package ru.maramzin.shopapp.client.business.converter.domain;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.maramzin.shopapp.client.business.converter.entity.ClientConverter;
import ru.maramzin.shopapp.client.business.domain.FindClient;
import ru.maramzin.shopapp.client.business.dto.request.FindClientRequest;
import ru.maramzin.shopapp.client.business.dto.response.success.FindClientResponse;
import ru.maramzin.shopapp.client.business.persistence.FindClientRepository;

@Service
@RequiredArgsConstructor
public class FindClientConverter implements
    DomainConverter<FindClientRequest, FindClient, FindClientResponse> {

  private final FindClientRepository repository;
  private final ClientConverter clientConverter;

  @Override
  public FindClient convertToDomain(FindClientRequest request) {
    return FindClient.builder()
        .client(repository.findById(request.getClientId()).orElse(null))
        .build();
  }

  @Override
  public FindClientResponse convertToResponse(FindClient domain) {
    return FindClientResponse.builder()
        .clientInfo(clientConverter.toResponse(domain.getClient()))
        .build();
  }
}
