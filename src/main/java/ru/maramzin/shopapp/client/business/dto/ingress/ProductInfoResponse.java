package ru.maramzin.shopapp.client.business.dto.ingress;

import java.util.UUID;
import ru.maramzin.shopapp.client.business.dto.ingress.enumeration.ClothesCategory;
import ru.maramzin.shopapp.client.business.dto.ingress.enumeration.SeasonCategory;

public record ProductInfoResponse(UUID id, String name, String description, Integer cost,
                                  Integer discount, Integer quantity,
                                  ClothesCategory clothesCategory, SeasonCategory seasonCategory,
                                  UUID sellerId) {

}
