package ru.maramzin.shopapp.client.business.persistence;

import java.util.Optional;
import java.util.UUID;
import ru.maramzin.shopapp.client.business.domain.AddItem;
import ru.maramzin.shopapp.client.business.entity.Client;

public interface AddItemRepository extends Storage<AddItem>{

  Optional<Client> findClientById(UUID clientId);
}
