package ru.maramzin.shopapp.client.business.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.maramzin.shopapp.client.business.converter.domain.ExpireOrdersConverter;
import ru.maramzin.shopapp.client.business.domain.ExpireOrders;
import ru.maramzin.shopapp.client.business.dto.request.ExpireOrdersRequest;
import ru.maramzin.shopapp.client.business.dto.response.Result;
import ru.maramzin.shopapp.client.business.dto.response.failed.RequestFailed;
import ru.maramzin.shopapp.client.business.dto.response.success.ExpireOrdersResponse;
import ru.maramzin.shopapp.client.business.persistence.ExpireOrdersRepository;

@Service
@RequiredArgsConstructor
public class ExpireOrdersService implements RequestProcessor<ExpireOrdersRequest, ExpireOrdersResponse> {

  private final ExpireOrdersConverter converter;
  private final ExpireOrdersRepository repository;

  @Override
  @Transactional
  public Result<ExpireOrdersResponse, RequestFailed> process(ExpireOrdersRequest request) {
    ExpireOrders domain = converter.convertToDomain(request)
        .validate()
        .execute()
        .ifSuccess(repository::persist)
        .rollbackIfFailed();

    return converter.convertToResult(domain);
  }
}
