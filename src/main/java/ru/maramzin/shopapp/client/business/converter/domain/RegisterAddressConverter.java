package ru.maramzin.shopapp.client.business.converter.domain;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.maramzin.shopapp.client.business.converter.entity.AddressConverter;
import ru.maramzin.shopapp.client.business.domain.RegisterAddress;
import ru.maramzin.shopapp.client.business.dto.request.RegisterAddressRequest;
import ru.maramzin.shopapp.client.business.dto.response.success.RegisterAddressResponse;

@Service
@RequiredArgsConstructor
public class RegisterAddressConverter implements DomainConverter<RegisterAddressRequest,
    RegisterAddress, RegisterAddressResponse> {

  private final AddressConverter addressConverter;

  @Override
  public RegisterAddress convertToDomain(RegisterAddressRequest request) {
    return RegisterAddress.builder()
        .request(request)
        .build();
  }

  @Override
  public RegisterAddressResponse convertToResponse(RegisterAddress domain) {
    return RegisterAddressResponse.builder()
        .addressInfo(addressConverter.toResponse(domain.getAddress()))
        .build();
  }
}
