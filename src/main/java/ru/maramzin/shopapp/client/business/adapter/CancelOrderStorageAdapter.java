package ru.maramzin.shopapp.client.business.adapter;

import ru.maramzin.shopapp.client.business.domain.CancelOrder;

public interface CancelOrderStorageAdapter {

  void rollbackPurchase(CancelOrder domain);
}
