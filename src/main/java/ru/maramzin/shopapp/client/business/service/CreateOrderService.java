package ru.maramzin.shopapp.client.business.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.maramzin.shopapp.client.business.adapter.CreateOrderStorageAdapter;
import ru.maramzin.shopapp.client.business.domain.CreateOrder;
import ru.maramzin.shopapp.client.business.dto.request.CreateOrderRequest;
import ru.maramzin.shopapp.client.business.dto.response.Result;
import ru.maramzin.shopapp.client.business.dto.response.failed.RequestFailed;
import ru.maramzin.shopapp.client.business.dto.response.success.CreateOrderResponse;
import ru.maramzin.shopapp.client.business.persistence.CreateOrderRepository;
import ru.maramzin.shopapp.client.business.converter.domain.CreateOrderConverter;

@Service
@RequiredArgsConstructor
public class CreateOrderService implements RequestProcessor<CreateOrderRequest, CreateOrderResponse> {

  private final CreateOrderRepository repository;
  private final CreateOrderConverter converter;
  private final CreateOrderStorageAdapter adapter;

  @Override
  @Transactional
  public Result<CreateOrderResponse, RequestFailed> process(CreateOrderRequest request) {
    CreateOrder createOrder = converter.convertToDomain(request)
        .validate()
        .execute()
        .ifSuccess(adapter::purchase)
        .ifSuccess(repository::persist)
        .rollbackIfFailed();

    return converter.convertToResult(createOrder);
  }
}
