package ru.maramzin.shopapp.client.business.persistence;

import java.time.LocalDateTime;
import java.util.List;
import ru.maramzin.shopapp.client.business.domain.ExpireOrders;
import ru.maramzin.shopapp.client.business.entity.Order;

public interface ExpireOrdersRepository extends Storage<ExpireOrders> {

  List<Order> findOrdersToExpire(LocalDateTime maxExpireDateTime);
}
