package ru.maramzin.shopapp.client.business.persistence;

import ru.maramzin.shopapp.client.business.domain.BaseDomain;

public interface Storage<D extends BaseDomain<D>> {

  void persist(D domain);
}
