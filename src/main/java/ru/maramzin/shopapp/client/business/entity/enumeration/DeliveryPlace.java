package ru.maramzin.shopapp.client.business.entity.enumeration;

public enum DeliveryPlace {

  HOME, POST_OFFICE
}
