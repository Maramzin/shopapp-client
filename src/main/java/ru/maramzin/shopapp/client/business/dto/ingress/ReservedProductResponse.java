package ru.maramzin.shopapp.client.business.dto.ingress;

public record ReservedProductResponse(ProductInfoResponse productInfo, Integer quantity) {
}
