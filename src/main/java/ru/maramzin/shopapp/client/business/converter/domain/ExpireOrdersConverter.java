package ru.maramzin.shopapp.client.business.converter.domain;

import java.time.LocalDateTime;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.client.business.converter.entity.OrderConverter;
import ru.maramzin.shopapp.client.business.domain.ExpireOrders;
import ru.maramzin.shopapp.client.business.dto.request.ExpireOrdersRequest;
import ru.maramzin.shopapp.client.business.dto.response.success.ExpireOrdersResponse;
import ru.maramzin.shopapp.client.business.persistence.ExpireOrdersRepository;

@Component
@RequiredArgsConstructor
public class ExpireOrdersConverter implements
    DomainConverter<ExpireOrdersRequest, ExpireOrders, ExpireOrdersResponse> {

  private final OrderConverter orderConverter;
  private final ExpireOrdersRepository expireOrdersRepository;

  @Value("${rules.order-expire-time}")
  private Integer expireTimeDays;

  @Override
  public ExpireOrders convertToDomain(ExpireOrdersRequest request) {
    LocalDateTime maxCreationDateTime = request.getCurrentDateTime().minusDays(expireTimeDays);
    return ExpireOrders.builder()
        .orders(expireOrdersRepository.findOrdersToExpire(maxCreationDateTime))
        .build();
  }

  @Override
  public ExpireOrdersResponse convertToResponse(ExpireOrders domain) {
    return ExpireOrdersResponse.builder()
        .orderResponses(domain.getOrders().stream()
            .map(orderConverter::toResponse)
            .toList())
        .build();
  }
}
