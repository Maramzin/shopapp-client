package ru.maramzin.shopapp.client.business.utils.conditional;

import java.util.function.Consumer;

public interface ThrowableConditional extends Conditional {

  ThrowableConditional onFailure(Consumer<Throwable> consumer);
}
