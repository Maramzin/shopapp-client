package ru.maramzin.shopapp.client.application.scheduler;

import java.time.LocalDateTime;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.client.business.dto.request.ExpireOrdersRequest;
import ru.maramzin.shopapp.client.business.service.ExpireOrdersService;

@Component
@RequiredArgsConstructor
public class StartDayScheduler {

  private final ExpireOrdersService expireOrdersService;

  @Scheduled(cron = "0 0 0 * * ?")
  public void expireOrders() {
    expireOrdersService.process(new ExpireOrdersRequest(LocalDateTime.now()));
  }
}
