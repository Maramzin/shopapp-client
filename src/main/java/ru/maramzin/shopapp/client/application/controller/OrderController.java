package ru.maramzin.shopapp.client.application.controller;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.maramzin.shopapp.client.business.dto.request.CancelOrderRequest;
import ru.maramzin.shopapp.client.business.dto.request.CreateOrderRequest;
import ru.maramzin.shopapp.client.business.dto.request.ExpireOrdersRequest;
import ru.maramzin.shopapp.client.business.dto.request.PayOrderRequest;
import ru.maramzin.shopapp.client.business.dto.response.Result;
import ru.maramzin.shopapp.client.business.dto.response.failed.RequestFailed;
import ru.maramzin.shopapp.client.business.dto.response.success.CancelOrderResponse;
import ru.maramzin.shopapp.client.business.dto.response.success.CreateOrderResponse;
import ru.maramzin.shopapp.client.business.dto.response.success.PayOrderResponse;
import ru.maramzin.shopapp.client.business.service.CancelOrderService;
import ru.maramzin.shopapp.client.business.service.CreateOrderService;
import ru.maramzin.shopapp.client.business.service.ExpireOrdersService;
import ru.maramzin.shopapp.client.business.service.PayOrderService;

@RestController
@RequestMapping("/order")
@RequiredArgsConstructor
public class OrderController {

  private final CreateOrderService createOrderService;
  private final PayOrderService payOrderService;
  private final CancelOrderService cancelOrderService;

  private final ExpireOrdersService expireOrdersService;

  @PutMapping("/create")
  public ResponseEntity<?> createOrder(@RequestBody CreateOrderRequest request) {
    Result<CreateOrderResponse, RequestFailed> result = createOrderService.process(request);
    if (result.isSuccess()) {
      return ResponseEntity.ok(result.getSuccess());
    } else {
      return ResponseEntity.badRequest().body(result.getFailed());
    }
  }

  @PostMapping("/pay")
  public ResponseEntity<?> payOrder(@RequestParam UUID orderId) {
    PayOrderRequest request = new PayOrderRequest(orderId);
    Result<PayOrderResponse, RequestFailed> result = payOrderService.process(request);
    if(result.isSuccess()) {
      return ResponseEntity.ok(result.getSuccess());
    } else {
      return ResponseEntity.badRequest().body(result.getFailed());
    }
  }

  @PostMapping("/cancel")
  public ResponseEntity<?> cancelOrder(@RequestParam UUID orderId) {
    CancelOrderRequest request = new CancelOrderRequest(orderId);
    Result<CancelOrderResponse, RequestFailed> result = cancelOrderService.process(request);
    if(result.isSuccess()) {
      return ResponseEntity.ok(result.getSuccess());
    } else {
      return ResponseEntity.badRequest().body(result.getFailed());
    }
  }

  @PostMapping("/expire")
  public ResponseEntity<?> expireOrders(@RequestParam(required = false) LocalDateTime dateTime) {
    ExpireOrdersRequest request = new ExpireOrdersRequest(Optional.ofNullable(dateTime)
        .orElse(LocalDateTime.now()));

    return successOrFailed(expireOrdersService.process(request));
  }

  private ResponseEntity<?> successOrFailed(Result<?,?> result) {
    if(result.isSuccess()) {
      return ResponseEntity.ok(result.getSuccess());
    } else {
      return ResponseEntity.badRequest().body(result.getFailed());
    }
  }
}
