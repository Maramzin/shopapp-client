package ru.maramzin.shopapp.client.application.controller;

import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.maramzin.shopapp.client.business.dto.request.AddItemRequest;
import ru.maramzin.shopapp.client.business.dto.request.FindClientRequest;
import ru.maramzin.shopapp.client.business.dto.request.RegisterClientRequest;
import ru.maramzin.shopapp.client.business.dto.request.ReplenishBalanceRequest;
import ru.maramzin.shopapp.client.business.dto.response.Result;
import ru.maramzin.shopapp.client.business.dto.response.failed.RequestFailed;
import ru.maramzin.shopapp.client.business.dto.response.success.AddItemResponse;
import ru.maramzin.shopapp.client.business.dto.response.success.FindClientResponse;
import ru.maramzin.shopapp.client.business.dto.response.success.RegisterClientResponse;
import ru.maramzin.shopapp.client.business.dto.response.success.ReplenishBalanceResponse;
import ru.maramzin.shopapp.client.business.service.AddItemService;
import ru.maramzin.shopapp.client.business.service.FindClientService;
import ru.maramzin.shopapp.client.business.service.RegisterClientService;
import ru.maramzin.shopapp.client.business.service.ReplenishBalanceService;

@RestController
@RequestMapping("/client")
@RequiredArgsConstructor
public class ClientController {

  private final RegisterClientService registerClientService;
  private final AddItemService addItemService;
  private final FindClientService findClientService;
  private final ReplenishBalanceService replenishBalanceService;

  @PutMapping("/register")
  public ResponseEntity<?> register(@RequestBody RegisterClientRequest request) {
    Result<RegisterClientResponse, RequestFailed> result = registerClientService.process(request);
    if (result.isSuccess()) {
      return ResponseEntity.ok(result.getSuccess());
    } else {
      return ResponseEntity.badRequest().body(result.getFailed());
    }
  }

  @PostMapping("/addItem")
  public ResponseEntity<?> addItem(@RequestBody AddItemRequest request) {
    Result<AddItemResponse, RequestFailed> result = addItemService.process(request);
    if (result.isSuccess()) {
      return ResponseEntity.ok(result.getSuccess());
    } else {
      return ResponseEntity.badRequest().body(result.getFailed());
    }
  }

  @GetMapping("/find")
  public ResponseEntity<?> findById(@RequestParam UUID clientId) {
    FindClientRequest request = new FindClientRequest(clientId);
    Result<FindClientResponse, RequestFailed> result = findClientService.process(request);
    if (result.isSuccess()) {
      return ResponseEntity.ok(result.getSuccess());
    } else {
      return ResponseEntity.badRequest().body(result.getFailed());
    }
  }

  @PostMapping("/balance/replenish")
  public ResponseEntity<?> replenishBalance(@RequestBody ReplenishBalanceRequest request) {
    Result<ReplenishBalanceResponse, RequestFailed> result = replenishBalanceService.process(request);
    if (result.isSuccess()) {
      return ResponseEntity.ok(result.getSuccess());
    } else {
      return ResponseEntity.badRequest().body(result.getFailed());
    }
  }
}
