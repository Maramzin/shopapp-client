package ru.maramzin.shopapp.client.infrastructure.repository.impl;

import java.util.Optional;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.client.business.entity.Client;
import ru.maramzin.shopapp.client.business.persistence.FindClientRepository;
import ru.maramzin.shopapp.client.infrastructure.repository.jpa.ClientRepository;

@Component
@RequiredArgsConstructor
public class FindClientRepositoryImpl implements FindClientRepository {

  private final ClientRepository repository;

  @Override
  public Optional<Client> findById(UUID id) {
    return repository.findById(id);
  }
}
