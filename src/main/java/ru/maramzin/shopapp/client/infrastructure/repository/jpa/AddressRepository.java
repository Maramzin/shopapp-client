package ru.maramzin.shopapp.client.infrastructure.repository.jpa;

import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.maramzin.shopapp.client.business.entity.Address;

public interface AddressRepository extends JpaRepository<Address, UUID> {

}
