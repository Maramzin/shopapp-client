package ru.maramzin.shopapp.client.infrastructure.repository.impl;

import java.util.Optional;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.client.business.domain.CancelOrder;
import ru.maramzin.shopapp.client.business.entity.Order;
import ru.maramzin.shopapp.client.business.persistence.CancelOrderRepository;
import ru.maramzin.shopapp.client.infrastructure.repository.jpa.OrderRepository;

@Component
@RequiredArgsConstructor
public class CancelOrderRepositoryImpl implements CancelOrderRepository {

  private final OrderRepository orderRepository;

  @Override
  public Optional<Order> findById(UUID id) {
    return orderRepository.findById(id);
  }

  @Override
  public void persist(CancelOrder domain) {
    orderRepository.save(domain.getOrder());
  }
}
