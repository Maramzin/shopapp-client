package ru.maramzin.shopapp.client.infrastructure.adapter.client;

import java.util.List;
import java.util.UUID;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import ru.maramzin.shopapp.client.business.dto.egress.PurchaseRequest;
import ru.maramzin.shopapp.client.business.dto.ingress.PurchaseResponseWrapper;
import ru.maramzin.shopapp.client.business.dto.ingress.ReservationResponse;

@FeignClient(value = "shopapp-storage", url = "${rest.storage.url}")
public interface StorageClient {

  @PostMapping("/api/v1/product/purchase/reserve")
  PurchaseResponseWrapper reservePurchase(@RequestBody List<PurchaseRequest> purchaseRequests,
      @RequestParam UUID clientId);

  @PostMapping("/api/v1/product/purchase/rollback")
  void rollbackPurchase(@RequestParam UUID reservationId);

  @PostMapping("/api/v1/product/purchase/commit")
  void commitPurchase(@RequestParam UUID reservationId);

  @GetMapping("/api/v1/reservation/find")
  ReservationResponse findById(@RequestParam UUID id);
}
