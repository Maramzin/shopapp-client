package ru.maramzin.shopapp.client.infrastructure.repository.impl;

import java.time.LocalDateTime;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.client.business.domain.ExpireOrders;
import ru.maramzin.shopapp.client.business.entity.Order;
import ru.maramzin.shopapp.client.business.persistence.ExpireOrdersRepository;
import ru.maramzin.shopapp.client.infrastructure.repository.jpa.OrderRepository;

@Component
@RequiredArgsConstructor
public class ExpireOrdersRepositoryImpl implements ExpireOrdersRepository {

  private final OrderRepository orderRepository;

  @Override
  public List<Order> findOrdersToExpire(LocalDateTime maxExpireDateTime) {
    return orderRepository.findOrdersToExpire(maxExpireDateTime);
  }

  @Override
  public void persist(ExpireOrders domain) {
    domain.getOrders().forEach(orderRepository::save);
  }
}
