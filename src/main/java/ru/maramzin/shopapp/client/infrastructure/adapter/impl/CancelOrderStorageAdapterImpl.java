package ru.maramzin.shopapp.client.infrastructure.adapter.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.client.business.adapter.CancelOrderStorageAdapter;
import ru.maramzin.shopapp.client.business.domain.CancelOrder;
import ru.maramzin.shopapp.client.infrastructure.adapter.client.StorageClient;

@Component
@RequiredArgsConstructor
public class CancelOrderStorageAdapterImpl implements CancelOrderStorageAdapter {

  private final StorageClient storageClient;

  @Override
  public void rollbackPurchase(CancelOrder domain) {
    storageClient.rollbackPurchase(domain.getOrder().getReservationId());
  }
}
