package ru.maramzin.shopapp.client.infrastructure.adapter.impl;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.client.business.adapter.CreateOrderStorageAdapter;
import ru.maramzin.shopapp.client.business.converter.egress.PurchaseConverter;
import ru.maramzin.shopapp.client.business.domain.CreateOrder;
import ru.maramzin.shopapp.client.business.dto.egress.PurchaseRequest;
import ru.maramzin.shopapp.client.business.dto.ingress.PurchaseResponse;
import ru.maramzin.shopapp.client.business.dto.ingress.PurchaseResponseWrapper;
import ru.maramzin.shopapp.client.business.entity.Client;
import ru.maramzin.shopapp.client.business.entity.Order;
import ru.maramzin.shopapp.client.business.entity.enumeration.PaymentState;
import ru.maramzin.shopapp.client.business.utils.DiscountUtils;
import ru.maramzin.shopapp.client.business.utils.conditional.Conditional;
import ru.maramzin.shopapp.client.business.utils.exception.NotEnoughBalance;
import ru.maramzin.shopapp.client.infrastructure.adapter.client.StorageClient;

@Component
@RequiredArgsConstructor
public class CreateOrderStorageAdapterImpl implements CreateOrderStorageAdapter {

  private final StorageClient storageClient;
  private final PurchaseConverter purchaseConverter;

  @Override
  public void purchase(CreateOrder domain) throws NotEnoughBalance {
    Order order = domain.getOrder();
    Client client = domain.getClient();

    List<PurchaseRequest> purchaseRequests = domain.getOrder()
        .getCart()
        .getItems()
        .stream()
        .map(purchaseConverter::toResponse)
        .toList();

    PurchaseResponseWrapper responseWrapper =
        storageClient.reservePurchase(purchaseRequests, client.getId());
    order.setReservationId(responseWrapper.getReservationId());

    Conditional.of(order.getPaymentState() == PaymentState.PAID)
        .then(() -> {
          int totalCost = responseWrapper.getContent().stream()
              .mapToInt(this::getFinalCost)
              .sum();

          Conditional.of(totalCost <= client.getBalance())
              .then(() -> {
                storageClient.commitPurchase(responseWrapper.getReservationId());
                client.setBalance(client.getBalance() - totalCost);
              })
              .orElse(() -> storageClient.rollbackPurchase(responseWrapper.getReservationId()))
              .orElseThrow(new NotEnoughBalance("Not enough money on balance: " +
                  client.getBalance() + ", required: " + totalCost + "."));
        });
  }

  private Integer getFinalCost(PurchaseResponse purchase) {
    return DiscountUtils.discountedPrice(purchase.cost(), purchase.discount()) * purchase.quantity();
  }
}
