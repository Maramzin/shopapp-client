package ru.maramzin.shopapp.client.infrastructure.repository.impl;

import java.util.Optional;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.client.business.domain.CreateOrder;
import ru.maramzin.shopapp.client.business.entity.Client;
import ru.maramzin.shopapp.client.business.persistence.CreateOrderRepository;
import ru.maramzin.shopapp.client.infrastructure.repository.jpa.ClientRepository;
import ru.maramzin.shopapp.client.infrastructure.repository.jpa.OrderRepository;

@Component
@RequiredArgsConstructor
public class CreateOrderRepositoryImpl implements CreateOrderRepository {

  private final OrderRepository orderRepository;
  private final ClientRepository clientRepository;

  @Override
  public void persist(CreateOrder domain) {
    orderRepository.save(domain.getOrder());
    clientRepository.save(domain.getClient());
  }

  @Override
  public Optional<Client> findClientById(UUID id) {
    return clientRepository.findById(id);
  }
}
